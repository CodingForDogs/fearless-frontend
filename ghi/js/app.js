function createCard(name, description, pictureUrl, starts, ends, location) {
  return `
    <div class="col-4 border-2 shadow mx-auto px-0 mb-auto pb-auto"
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer bg-light">
          <small class="text-muted">${starts} - ${ends}</small>
        </div>
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';
  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw 'Error! Something went wrong';
      // Figure out what to do when the response is bad
    } else {

      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {

          const details = await detailResponse.json();
          const title = details.conference.name;
          const locationName = details.conference.location.name;
          const description = details.conference.description;

          const pictureUrl = details.conference.location.picture_url;

          const startDate = new Date(details.conference.starts);
          const starts = startDate.toLocaleDateString();
          const endDate = new Date(details.conference.ends);
          const ends = endDate.toLocaleDateString(startDate);

          const html = createCard(title, description, pictureUrl, starts, ends, locationName);
          const column = document.querySelector('.row');
          column.innerHTML += html;
        }
      }

    }
  } catch (e) {
    console.error(e);
    // Figure out what to do if an error is raised
  }

});

console.log(123);
